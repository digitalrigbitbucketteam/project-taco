Project taco
=====================


Install the dependencies and start the server.

```
npm install
npm start
open http://localhost:8080
```
```

### Dependencies

* React
* Webpack
* [babel-loader](https://github.com/babel/babel-loader)
* [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
